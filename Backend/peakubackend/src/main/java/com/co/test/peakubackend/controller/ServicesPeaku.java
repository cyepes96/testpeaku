package com.co.test.peakubackend.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.co.test.peakubackend.model.Producto;
import com.co.test.peakubackend.model.Proveedor;
import com.co.test.peakubackend.repository.ProductoRepository;
import com.co.test.peakubackend.repository.ProveedorRepository;
import com.google.gson.Gson;

@RestController
public class ServicesPeaku {

	@Autowired
	private ProveedorRepository proveedorRepository;

	@Autowired
	private ProductoRepository productoRepository;

	@RequestMapping(value = "/getMajorByPrice", method = RequestMethod.GET)
	public String getMajorByPrice() {
		List<Producto> result = new ArrayList<Producto>();
		Iterable<Producto> iterable = productoRepository.findPriceMajor(180000);

		for (Producto current : iterable) {
			Producto p = new Producto();
			p.setNombre(current.getNombre());
			p.setPrecio(current.getPrecio());
			result.add(p);
		}

		return new Gson().toJson(result);
	}

	@RequestMapping(value = "/getPriceBetween", method = RequestMethod.GET)
	public String getPriceBetween() {
		List<Producto> result = new ArrayList<Producto>();
		Iterable<Producto> iterable = productoRepository.findPriceBetween(20000, 180000);

		for (Producto current : iterable) {
			Producto p = new Producto();
			p.setNombre(current.getNombre());
			p.setPrecio(current.getPrecio());
			result.add(p);
		}

		return new Gson().toJson(result);
	}

	@RequestMapping(value = "/getProductByNit", method = RequestMethod.GET)
	public String getProductByNit() {
		List<Producto> result = new ArrayList<Producto>();
		Iterable<Producto> iterable = productoRepository.findProductByNit("123456");

		for (Producto current : iterable) {
			Producto p = new Producto();
			p.setNombre(current.getNombre());
			result.add(p);
		}

		return new Gson().toJson(result);
	}

	@RequestMapping(value = "/getProviderOfProduct", method = RequestMethod.GET)
	public String getProviderOfProduct() {
		List<Proveedor> result = new ArrayList<Proveedor>();
		Iterable<Proveedor> iterable = proveedorRepository.findProviderOfProduct("PRD1");

		for (Proveedor current : iterable) {
			Proveedor p = new Proveedor();
			p.setNombre(current.getNombre());
			result.add(p);
		}

		return new Gson().toJson(result);
	}

}
