package com.co.test.peakubackend.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.co.test.peakubackend.model.Producto;

public interface ProductoRepository extends CrudRepository<Producto, String> {

	@Query(value = "select codigo, nit_proveedor, nombre, precio from producto where precio >= ?1 order by precio desc", nativeQuery = true)
	public Iterable<Producto> findPriceMajor(int precio);

	@Query(value = "select codigo, nit_proveedor, nombre, precio from producto where precio >= ?1 and precio <= ?2", nativeQuery = true)
	public Iterable<Producto> findPriceBetween(int lowerPrice, int higherPrice);

	@Query(value = "select pd.codigo, pd.nit_proveedor, pd.nombre, pd.precio from producto pd inner join proveedor pv on pv.nit = pd.nit_proveedor where pd.nit_proveedor = ?1", nativeQuery = true)
	public Iterable<Producto> findProductByNit(String nit);
}
