package com.co.test.peakubackend.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

@Entity
public class Proveedor {

	@Id
	@Length(min = 9, max = 15)
	private String nit;
	@NotEmpty
	@Length(min = 5, max = 50)
	private String nombre;
	@NotEmpty
	@Length(min = 7, max = 10)
	private String telefono;
	@NotEmpty
	@Length(max = 50)
	@Email
	private String email;
	@OneToMany(mappedBy = "proveedor")
	private List<Producto> productos;

	public Proveedor() {
		super();
	}

	public Proveedor(String nit, String nombre, String telefono, String email) {
		this.nit = nit;
		this.nombre = nombre;
		this.telefono = telefono;
		this.email = email;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}
}
