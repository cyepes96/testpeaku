package com.co.test.peakubackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

@Entity
public class Producto {

	@Id
	@Length(max = 8)
	private String codigo;
	@NotEmpty
	@Length(min = 9, max = 15)
	@Column(name = "nit_proveedor")
	private String nitProveedor;
	@NotEmpty
	@Length(max = 11)
	private String precio;
	@NotEmpty
	@Length(min = 5, max = 50)
	@Email
	private String nombre;
	@ManyToOne
	@JoinColumn(name = "nit_proveedor", insertable = false, updatable = false)
	private Proveedor proveedor;

	public Producto() {
		super();
	}

	public Producto(String codigo, String nitProveedor, String precio, @Email String nombre) {
		this.codigo = codigo;
		this.nitProveedor = nitProveedor;
		this.precio = precio;
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNitProveedor() {
		return nitProveedor;
	}

	public void setNitProveedor(String nitProveedor) {
		this.nitProveedor = nitProveedor;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
}
