package com.co.test.peakubackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PeakubackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PeakubackendApplication.class, args);
	}

}
