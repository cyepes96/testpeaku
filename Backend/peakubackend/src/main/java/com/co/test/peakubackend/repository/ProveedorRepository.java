package com.co.test.peakubackend.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.co.test.peakubackend.model.Proveedor;

public interface ProveedorRepository extends CrudRepository<Proveedor, String> {

	@Query(value = "select pv.nit, pv.telefono, pv.nombre, pv.email from producto pd inner join proveedor pv on pv.nit = pd.nit_proveedor where pd.codigo = ?1", nativeQuery = true)
	public Iterable<Proveedor> findProviderOfProduct(String nit);

}
